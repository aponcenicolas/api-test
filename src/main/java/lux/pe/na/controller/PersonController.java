package lux.pe.na.controller;

import lombok.AllArgsConstructor;
import lux.pe.na.controller.response.PersonResponse;
import lux.pe.na.model.dto.PersonDto;
import lux.pe.na.repository.PersonRepository;
import lux.pe.na.service.PersonService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.util.List;

@RestController
@RequestMapping("/api/people")
@AllArgsConstructor
public class PersonController {

  private final PersonService personService;
  private final PersonRepository personRepository;

  @GetMapping
  public Mono<PersonResponse> list() {
    return Mono.just(PersonResponse.builder()
        .code(200)
        .message("success")
        .data(personService.findAllPeople())
        .build());
  }

  @GetMapping("/{id}")
  public PersonResponse get(@PathVariable Long id) {
    return PersonResponse.builder().code(200).message("success").data(personRepository.findById(id)).build();
  }

}
