package lux.pe.na.controller.response;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonResponse {
  private Integer code;
  private String message;
  private Object data;
}
