package lux.pe.na.mapper;

import lux.pe.na.model.dto.PersonDto;
import lux.pe.na.model.entity.Person;

public final class PersonMapper {

  public static Person personDtoToPerson(PersonDto personDto) {
    return Person.builder()
        .id(personDto.getId())
        .name(personDto.getName())
        .lastname(personDto.getLastname())
        .age(personDto.getAge())
        .phone(personDto.getPhone())
        .address(personDto.getAddress())
        .build();
  }

  public static PersonDto personToPersonDto(Person person) {
    return PersonDto.builder()
        .id(person.getId())
        .name(person.getName())
        .lastname(person.getLastname())
        .age(person.getAge())
        .phone(person.getPhone())
        .address(person.getAddress())
        .build();
  }
}
