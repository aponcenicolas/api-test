package lux.pe.na.model.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonDto {

  private Long id;
  private String name;
  private String lastname;
  private Integer age;
  private String phone;
  private String address;
}
