package lux.pe.na.service;

import lux.pe.na.model.dto.PersonDto;
import reactor.core.publisher.Mono;

import java.util.List;

public interface PersonService {
  Mono<List<PersonDto>> findAllPeople();
}
