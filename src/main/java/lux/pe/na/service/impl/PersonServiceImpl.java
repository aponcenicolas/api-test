package lux.pe.na.service.impl;

import lombok.AllArgsConstructor;
import lux.pe.na.mapper.PersonMapper;
import lux.pe.na.model.dto.PersonDto;
import lux.pe.na.repository.PersonRepository;
import lux.pe.na.service.PersonService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class PersonServiceImpl implements PersonService {

  private final PersonRepository personRepository;

  @Override
  public Mono<List<PersonDto>> findAllPeople() {
    return Mono.just(personRepository.findAll()
        .stream()
        .map(PersonMapper::personToPersonDto)
        .collect(Collectors.toList()));
  }
}
